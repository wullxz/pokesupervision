var map = L.map('mapdiv').setView([49.95155, 7.924318], 16);
var marker = L.marker([0, 0]).addTo(map);
var scanpts = [];
var scancircles = [];

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

$('#searchbtn').click(function(e) {
  if (e) { e.stopPropagation(); }
  $('#searchbtn').hide();
  $('#search').slideDown('fast');
  $('#addr').focus();
});

$("#addr").keyup(function(event){
  if(event.keyCode == 13){
    addr_search();
  }
});

$('.mapoverlay').click(function(e) {
  e.stopPropagation();
});

$('#search').click(function (e) {
  e.stopPropagation();
});

//$('#closesearch').click(hideSearch);

function hideSearch() {
  $('#search').slideUp('fast');
  $('#searchbtn').show();
}

function addr_search() {
  var inp = document.getElementById("addr");

  $.getJSON('http://nominatim.openstreetmap.org/search?format=json&limit=5&q=' + inp.value, function(data) {

      var items = [];

      $.each(data, function(key, val) {
        items.push(
            "<li><a href='#' class='choseAddrLink' data-lat='" +
            val.lat + "' data-lng='" + val.lon + "'>" + val.display_name +
            '</a></li>'
            );
      });
      $('#results').empty();
      if (items.length != 0) {
        $('<p>', { html: "Search results:" }).appendTo('#results');
        $('<ul/>', {
          'class': 'my-new-list',
          html: items.join('')
        }).appendTo('#results');
        $('.choseAddrLink').click(chooseAddr);
      } else {
        $('<p>', { html: "No results found" }).appendTo('#results');
      }
});
}

function chooseAddr(event) {
  console.log("chose address!");
  if (!event) {
    console.log('no event. returning!');
    return;
  }

  var lat = $(event.target).data('lat');
  var lng = $(event.target).data('lng');
  var type = 'none';
  var loc = new L.latLng(lat, lng);
  map.panTo(loc);

  if (type == 'city' || type == 'administrative') {
    map.setZoom(13);
  } else {
    map.setZoom(15);
  }
  hideSearch();
}

function mapClick(e) {
  if (e.latlng) {
    purgeMap();
    marker.setLatLng(e.latlng);
    $('#lat').val(e.latlng.lat);
    $('#lng').val(e.latlng.lng);
  }
}

function purgeMap() {
  scancircles.forEach(function (layer) {
    map.removeLayer(layer);
  });
  scancircles = [];
  scanpts = [];
}

function shoot() {
  purgeMap();
  var offset = 320/1000;
  var lat = $('#lat').val();
  var lng = $('#lng').val();
  var multi = 1;
  var latlng = L.latLng(lat, lng);

  var multipliers = intRange(-multi, multi);
  console.log(multipliers);
  multipliers.forEach(function (val) {
    var xoff = 0;
    var multis = multipliers;
    var midlatlng = latlng;
    if (val%2 !== 0) {
      xoff = -150/1000;
      multis = intRange(-multi, multi+1);
    }
    midlatlng = getOffset(midlatlng, xoff, val*offset);
    scanLine(midlatlng, multis, offset);
  });
}

function scanLine(midlatlng, multis, offset) {
  var circleSettings = {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5
  }

  multis.forEach(function(val) {
    var off = val * offset;
    var center = getOffset(midlatlng, off, 0);
    scanpts.push(center);
    scancircles.push(L.circle(center, 200, circleSettings).addTo(map));
  });
}

function getOffset(latlng, dx, dy) {
  var r_earth = 6378;
  var newLat = latlng.lat + (dy / r_earth) * (180 / Math.PI);
  var newLng = latlng.lng + (dx / r_earth) * (180 / Math.PI) / Math.cos(latlng.lat * Math.PI/180);

  var newLatlng = L.latLng(newLat, newLng);
  return newLatlng;
}

function intRange(lowEnd, highEnd) {
  var arr = [],
  c = highEnd - lowEnd + 1;
  while ( c-- ) {
    arr[c] = highEnd--
  }
  return arr;
}

map.on('click', mapClick);
$('#shoot').click(shoot);
